import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CommonPipesModule } from '@ngCommon/pipe/common-pipes.module';
import { TopPreComponent } from './top-pre/top-pre.component';

@NgModule({
  declarations: [
    AppComponent,
    TopPreComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CommonPipesModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
