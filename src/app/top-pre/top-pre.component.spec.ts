import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopPreComponent } from './top-pre.component';

describe('TopPreComponent', () => {
  let component: TopPreComponent;
  let fixture: ComponentFixture<TopPreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopPreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopPreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
