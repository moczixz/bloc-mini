import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TopLiveComponent } from './top-live.component';
import { CommonPipesModule } from 'ng-common/pipe/common-pipes.module';


@NgModule({
  declarations: [TopLiveComponent],
  imports: [
    CommonModule,
    //CommonPipesModule
  ],
  exports: [
    TopLiveComponent
  ]
})
export class TopLiveModule { }
