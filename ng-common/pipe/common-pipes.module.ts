import { NgModule } from "@angular/core";
import { DetachPipe } from './detach.pipe';

@NgModule({
  declarations: [
    DetachPipe
  ],
  exports: [
    DetachPipe
  ]
})
export class CommonPipesModule {
  
}