import { AppState } from '../app-state';
import { AddMatch } from './matches-actions';
import { Reducer } from '@logic/redux/interfaces/reducer.interface';
import { StateChange } from '@logic/redux/state-change';
//import { Reducer } from 'src/app/redux/interfaces/reducer.interface';
//import { StateChange } from 'src/app/redux/state-change';

export class MatchesReducer implements Reducer<AppState> {

    public reducer(state: StateChange<AppState>, action: any): StateChange<AppState> {
        if (action instanceof AddMatch) {
            const newState = new AppState();
            newState.propagateFromState(state.state);
            newState.matches = [...newState.matches, action.match];
            return new StateChange(newState, 'matches');
        }
        return state;
    }
}
