import { AppState } from './app-state';
import { Singleton } from '@logic/di-resolver/di-resolver';

@Singleton([])
export class StoreConfig {

  public initialState = new AppState();

  public reducers = [];
  
}
