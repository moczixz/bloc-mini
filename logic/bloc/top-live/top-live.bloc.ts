import { Store } from '@logic/redux/store';
import { AppState } from '@logic/store/app-state';
import { Provide } from '@logic/di-resolver/di-resolver';

@Provide([Store])
export class TopLiveBloc {

  constructor(private store: Store<AppState>) {
  }

}
