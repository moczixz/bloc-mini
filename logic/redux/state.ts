export abstract class State<T> {

    private isFunction(obj: any): boolean {
        return !!(obj && obj.constructor && obj.call && obj.apply);
    }

    public propagateFromState(state: T): void {
        // is this mutate or not mutate??
        const self: object = this;
        Object.keys(state).forEach((key: string) => {
            if (this.isFunction(state[key]) && self.hasOwnProperty(key)) {
                self[key] = state[key];
            }
        });
    }
}
