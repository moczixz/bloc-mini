import { StateChange } from '../state-change';

export interface Reducer<T> {
    reducer(state: StateChange<T>, action: any): StateChange<T>;
}