import { Subject, Observable, EMPTY } from 'rxjs';
import { scan, shareReplay, startWith, map, filter, tap } from 'rxjs/operators';
import { StateChange } from './state-change';
import { Reducer } from './interfaces/reducer.interface';
import { StoreConfig } from '@logic/store/store-config';
import { Singleton } from '@logic/di-resolver/di-resolver';

interface OnChangeListener<T> {
  [prop: string]: Observable<T>;
}

@Singleton([StoreConfig])
export class Store<T> {
  private dispatcher$: Subject<any> = new Subject<any>();

  private onChangeListeners: OnChangeListener<T> = {};

  private internalState$ = this.dispatcher$
    .pipe(
      startWith(null),
      scan((state: StateChange<T>, action: any) => {
        return this.storeConfig.reducers.reduce((reducedState: StateChange<T>, reducer: Reducer<any>) =>
          reducer.reducer(reducedState, action), state
        );
      }, new StateChange(this.storeConfig.initialState, null)),
      shareReplay(1)
    );

  constructor(private storeConfig: StoreConfig) {
    this.internalState$.subscribe(console.log);
  }

  public dispatch(action: any): void {
    this.dispatcher$.next(action);
  }

  public get state$(): Observable<Readonly<T>> {
    return this.internalState$
      .pipe(
        map((state: StateChange<T>) => state.state)
      );
  }

  public selectField(prop: string): Observable<Readonly<T>> {
    if (!this.onChangeListeners[prop]) {
      this.onChangeListeners[prop] = this.internalState$
        .pipe(
          filter(state => state.prop === prop),
          map((state: StateChange<T>) => state.state),
          shareReplay(1)
        );
    }
    return this.onChangeListeners[prop];
  }

  public selectFieldById(prop: string, id: number|string): Observable<T> {
    return EMPTY;
  }

}


export function select<T, V>(selector: (state: T) => V) {
  return map((state: T) => selector(state));
}
