import { TopLiveBloc } from './bloc/top-live/top-live.bloc';
import { AuthBarBloc } from './bloc/auth-bar/auth-bar.bloc';
import { DiResolver } from './di-resolver/di-resolver';


export function getTopLiveBloc(): TopLiveBloc {
  return DiResolver.resolve(TopLiveBloc);
}

export function getAuthBloc() {
  return DiResolver.resolve(AuthBarBloc);
}