type ClassConstructor = any;

interface Registration {
  ctor: ClassConstructor,
  deps: Function[]
}

class DiContainer {
  private singletons: Function[] = [];
  private lazySingletons: Registration[] = [];
  private providers: Registration[] = [];

  private static instance: DiContainer;
  private constructor() {
  }
  static getInstance() {
      if (!DiContainer.instance) {
        DiContainer.instance = new DiContainer();
      }
      return DiContainer.instance;
  }

  public registerProvider(ctor: ClassConstructor, deps: Function[]): void {
    this.providers.push({
      ctor, deps
    })
  }

  public registerLazySingleton(ctor: ClassConstructor, deps: Function[]): void {
    this.lazySingletons.push({
      ctor, deps
    })
  }

  public registerSingleton(ctor: ClassConstructor, deps: Function[]): void {
    this.singletons.push(new ctor(...this.resolveDeps(deps)));
  }

  private resolveSingleton(dep: Function): Function {
    // Singleton instance is created at registration time
    const resolvedSingletonDep = this.singletons.find(singleton => singleton instanceof dep);
    if (resolvedSingletonDep) {
      return resolvedSingletonDep;
    }
    return null;
  }

  private resolveLazySingleton(dep: Function): Function {
    // lazy singleton instance is created at first time when needed and transform to singleton
    const lazySingletonIndex: number = this.lazySingletons.findIndex(lazy => lazy.ctor === dep);
    if (lazySingletonIndex !== -1) {
      const lazyCtor = this.lazySingletons[lazySingletonIndex].ctor;
      const lazyDeps = this.lazySingletons[lazySingletonIndex].deps;
      const resolvedLazySingleton = new lazyCtor(...this.resolveDeps(lazyDeps))
      this.singletons.push(resolvedLazySingleton);
      this.lazySingletons.splice(lazySingletonIndex, 1);
      return resolvedLazySingleton;
    }
    return null;
  }

  private resolveProvider(dep: Function): Function {
    // created each time when needed
    const foundProvider = this.providers.find(provider => provider.ctor === dep);
    const providerCtor = foundProvider.ctor as any;
    return new providerCtor(...this.resolveDeps(foundProvider.deps))
  }

  private resolveDeps(deps: Function[]): any {
    return deps.map((dep: Function) => {
      const resolvedSingleton = this.resolveSingleton(dep);
      if (resolvedSingleton) {
        return resolvedSingleton;
      }
      const resolvedLazySingleton = this.resolveLazySingleton(dep);
      if (resolvedLazySingleton) {
        return resolvedLazySingleton;
      } else {
        return this.resolveProvider(dep);
      }
    })
  }

  public resolve(ctor: any): any {
    const provider = this.providers.find(provider => provider.ctor === ctor);
    if (!provider) {
      console.log('cant find registered provider', ctor);
      return;
    }
    const resolvedDeps = this.resolveDeps(provider.deps);
    return new ctor(...resolvedDeps);
  }
}


export class DiResolver {

  public static registerProvider(ctor: Function, deps: Function[]): void {
    DiContainer.getInstance().registerProvider(ctor, deps);
  }

  public static registerSingleton(ctor: Function, deps: Function[]): void {
    DiContainer.getInstance().registerSingleton(ctor, deps);
  }

  public static registerLazySingleton(ctor: Function, deps: Function[]): void {
    DiContainer.getInstance().registerLazySingleton(ctor, deps);
  }
  
  public static resolve<T>(className: Function): T {
    return DiContainer.getInstance().resolve(className);
  }
}

export function Provide(deps: Function[]) {
  return (ctor: Function) => {
      DiResolver.registerProvider(ctor, deps);
  }
}

export function Singleton(deps: Function[]) {
  return (ctor: Function) => {
    DiResolver.registerSingleton(ctor, deps);
  }
}

export function LazySingleton(deps: Function[]) {
  return (ctor: Function) => {
    DiResolver.registerLazySingleton(ctor, deps);
  }
}